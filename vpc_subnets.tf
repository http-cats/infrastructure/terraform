resource "aws_subnet" "httpcats-http-az-a" {
  vpc_id            = aws_vpc.httpcats.id
  cidr_block        = "10.1.1.0/27"
  availability_zone = "ap-southeast-2a"

  tags = merge(local.common_tags, {
    "Name" = "httpcats AZ A"
  })
}

resource "aws_subnet" "httpcats-http-az-b" {
  vpc_id            = aws_vpc.httpcats.id
  cidr_block        = "10.1.1.32/27"
  availability_zone = "ap-southeast-2b"

  tags = merge(local.common_tags, {
    "Name" = "httpcats AZ B"
  })
}

resource "aws_route_table" "httpcats" {
  vpc_id = aws_vpc.httpcats.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.httpcats.id
  }

  tags = merge(local.common_tags, {
    "Name" = "httpcats Internet Route Table"
  })
}

resource "aws_route_table_association" "httpcats-http-az-a" {
  subnet_id      = aws_subnet.httpcats-http-az-a.id
  route_table_id = aws_route_table.httpcats.id
}

resource "aws_route_table_association" "httpcats-http-az-b" {
  subnet_id      = aws_subnet.httpcats-http-az-b.id
  route_table_id = aws_route_table.httpcats.id
}
