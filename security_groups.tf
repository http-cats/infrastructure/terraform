resource "aws_security_group" "alb" {
  name        = "httpcats-alb"
  description = "Manages all connections to the ALB"
  vpc_id      = aws_vpc.httpcats.id

  tags = merge(local.common_tags, {
    "Name" = "ALBs"
  })
}

resource "aws_security_group_rule" "alb-https" {
  type              = "ingress"
  description       = "HTTPS"
  from_port         = "443"
  to_port           = "443"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "egress_all" {
  type              = "egress"
  description       = "All"
  from_port         = "-1"
  to_port           = "-1"
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb.id
}


resource "aws_security_group" "webserver" {
  name        = "httpcats-webserver"
  description = "Manages all connections to the internal web servers"
  vpc_id      = aws_vpc.httpcats.id

  tags = merge(local.common_tags, {
    "Name" = "Webservers"
  })
}

resource "aws_security_group_rule" "http-custom" {
  type              = "ingress"
  description       = "custom http port"
  from_port         = "8080"
  to_port           = "8080"
  protocol          = "tcp"
  cidr_blocks       = [aws_vpc.httpcats.cidr_block]
  security_group_id = aws_security_group.webserver.id
}

resource "aws_security_group_rule" "ssh" {
  type              = "ingress"
  description       = "ssh"
  from_port         = "22"
  to_port           = "22"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.webserver.id
}

resource "aws_security_group_rule" "webserver-egress" {
  type              = "egress"
  description       = "All"
  from_port         = "-1"
  to_port           = "-1"
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.webserver.id
}
