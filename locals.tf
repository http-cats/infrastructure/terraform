locals {
  ubuntu_ami = "ami-0a43280cfb87ffdba"

  common_tags = {
    "Solution"    = "HTTP Cats"
    "Environment" = "MVP"
    "Contact"     = "Kenny"
  }
}
