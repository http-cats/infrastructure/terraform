resource "aws_internet_gateway" "httpcats" {
  vpc_id = aws_vpc.httpcats.id

  tags = merge(local.common_tags, {
    "Name" = "httpcats"
  })
}
