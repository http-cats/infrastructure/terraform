resource "aws_alb" "httpcats" {
  name = "httpcats-beta"

  subnets = [
    aws_subnet.httpcats-http-az-a.id,
    aws_subnet.httpcats-http-az-b.id,
  ]

  security_groups = [
    aws_security_group.alb.id
  ]

  tags = merge(local.common_tags, {
    "Name" = "HTTP DevOps Cats HTTP ALB"
  })
}

resource "aws_alb_listener" "http" {
  load_balancer_arn = aws_alb.httpcats.id
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate_validation.cert.certificate_arn

  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      status_code  = "404"
      message_body = "Meow?"
    }
  }
}

resource "aws_lb_listener_rule" "cats" {
  listener_arn = aws_alb_listener.http.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.cats.arn
  }

  condition {
    path_pattern {
      values = ["*"]
    }
  }
}

resource "aws_alb_target_group" "cats" {
  name     = "httpcats-web-servers"
  port     = "8080"
  protocol = "HTTP"
  vpc_id   = aws_vpc.httpcats.id

  tags = merge(local.common_tags, {
    "Name" = "HTTP DevOps Cats HTTP Servers"
  })
}

resource "aws_lb_target_group_attachment" "meow-a" {
  target_group_arn = aws_alb_target_group.cats.arn
  target_id        = aws_instance.meow_1.id
}

resource "aws_lb_target_group_attachment" "meow-b" {
  target_group_arn = aws_alb_target_group.cats.arn
  target_id        = aws_instance.meow_2.id
}
