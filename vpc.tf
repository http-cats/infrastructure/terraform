resource "aws_vpc" "httpcats" {
  cidr_block = "10.1.1.0/24"

  tags = merge(local.common_tags, {
    "Name" = "httpcats"
  })
}
